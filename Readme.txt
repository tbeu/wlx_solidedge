Solid Edge® Preview plugin 1.0.0.1 for Total Commander
======================================================

 * License:
-----------

This software is released as freeware.


 * Disclaimer:
--------------

This software is provided "AS IS" without any warranty, either expressed or
implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not be
liable for any special, incidental, consequential or indirect damages due to
loss of data or any other reason.


 * Installation:
----------------

Open the plugin archive using Total Commander and installation will start.
The default configuration file solidedge.ini is initialized during the very first
plugin usage.


 * Description:
---------------

Solid Edge stores a preview bitmap for CAD files.

The Solid Edge Preview plugin shows the embedded preview thumbnail of Solid Edge
CAD files. It can also be used for the thumbnail view of Total Commander >= 6.5.

Extracting the preview thumbnail does not require Solid Edge to be installed.

The default background color can be set in section [Solid Edge Preview Settings]
of solidedge.ini.


 * ChangeLog:
-------------

 o Version 1.0.0.1 (27.08.2019)
   - first build


 * References:
--------------

 o LS-Plugin Writer's Guide by Christian Ghisler
   - http://ghisler.fileburst.com/lsplugins/listplughelp2.1.zip


 * Trademark and Copyright Statements:
--------------------------------------

 o Solid Edge is a registered trademark of Siemens Product Lifecycle Management Software Inc.,
   or its subsidiaries or affiliates, in the United States and in other countries.
   - https://solidedge.siemens.com
 o Total Commander is Copyright © 1993-2019 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net